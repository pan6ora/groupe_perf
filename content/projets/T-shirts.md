---
title: "T-shirts"
categories:
  - "Projets"

# Theme-Defined params
thumbnail: "img/projets/t_shirts/T-shirts.webp" # Thumbnail image
lead: "Perdre une cotation, certe, mais avec classe !" # Lead text
comments: false # Enable Disqus comments for specific page
authorbox: false # Enable authorbox for specific page
pager: true # Enable pager navigation (prev/next) for specific page
toc: false # Enable Table of Contents for specific page
---

{{< load-photoswipe >}}

Les t-shirts du groupe perf sont là !

Un design personnalisé avec des surnoms tous plus cons les uns que les autres !

Bravo à Thomas pour le design, et à Gaétan pour la coordination des opérations.

## La genèse

Après une année passée ensemble, l'idée de faire des t-shirts pour le groupe est apparue tout naturellement.

Pour ce qui est du design, Thomas et Rémy se lancent rapidement dans la compétition.

Thomas grâce à ses talents de graphiste fait rapidement une première proposition.

{{< gallery >}}
  {{< figure src="img/projets/t_shirts/design_thomas.webp" >}}
{{< /gallery >}}

Rémy en bon informaticien open-source fait chauffer GIMP, et abouti après quelques jours de travail à un design inspiré du mouvement pictural de l'art naïf.

{{< gallery >}}
  {{< figure src="img/projets/t_shirts/design_remy.webp" >}}
{{< /gallery >}}

Cette proposition ne sera malheureusement pas retenue, sûrement trop avant-gardiste.

Trouver un surnom par personne ? Très peu pour nous ! On est pas une vulgaire liste BDE, et de toute façon ça aurait signifié faire un choix parmi plein de bonnes idées. Décision est donc prise de tous les mettre.

Reste alors à établir une liste de surnoms. Après très peu de débats mais beaucoup d'idées à la con, le résultat final est le suivant:


- **Manu:** 21h-22h, La roue libre, 19 ans et demi, Je reviens dans 15 min, J'ai une réu, C'est quoi ta séance?, Fan de course de cul, Venez on embête les gymnastes
- **Gaétan:** Flipper le requin, The crimpiste, 90°, 20 minutes, No crimp january, No feet no problem, Open source, CSU is my home
- **Rémy LC:** Rémy Albert ECO+, Coach mental pour dauphin, Twelve angry abs, No crimp january, SDF, Foot (ball) climber
- **Mathias:** Grimpeur Labo, Baby driver, Candy Up addict, Comp style is the only style, Stater c'est tricher
- **Merlin:** Merlouche, Le blond sous-côté, Grimpeur en kit, Dyno-saure, 500 kcal par mouv, CSU is the new RU
- **Thomas:** Thomal piniste, Coach mental pour dauphin, Boulder bro, Gonna stat this dyno, 2 pieds 80 chaussons, f(cake)=600Hz
- **Lilou:** CSUrvivor, La révélation, The return of the sleepcrusher, Rési life goal, Gonna stat this dyno
- **Noé:** Le major, Le chouchou, 7c reprise, Your project is my warm-up, Mental caillou
- **Dorian:** No feet no problem, Insultes moi !, Soudoyeur de prof, 60°, Gaétan junior
- **Brice:** No feet no problem, Chill crusher, Soudoyeur de prof, Less Peanuts more Pull-Ups
- **Gabin:** Le seul vrai grimpeur, 3 places ou infini, Thomal piniste's Microtraxion, L'artifeur, Probablement mort s'il ne sourit pas
- **Marjorie:** Almost no foot, Coach vitesse à béquilles, Tolérance infinie, No beer only burpees, Mental caillou, Co-pain
- **Nathan:** Infinity conti, L'albatros, The arquiste, Ice climber, 50 nuances de rési
- **Lionel:** 1-6-11, Four biceps, L'albatros, Holds breaker, The pinchiste
- **Rémy B:** Boulder Des Etudiants, Free brosses, Rémi Poirot ECO+, The absent, Ptite Bière ?
- **Louise:** CSUrvivor, Silent crusher, La force tranquille
- **Hugo:** Le +1 des +1, SDF, Chemise hawaïenne +1 cotation, Torse nu +2 cotation, Diagnostiquer c'est abdiquer
- **Coraline:** Heel hooker, Corachile talon, 180°, Rêve almost first ascent, Mental caillou, Co-pain
