---
title: "La cave"
categories:
  - "Projets"

# Theme-Defined params
thumbnail: "img/projets/cave/La_cave.webp" # Thumbnail image
lead: "Pourquoi aller en falaise quand on peut s'enfermer dans un cagibi ?" # Lead text
comments: false # Enable Disqus comments for specific page
authorbox: false # Enable authorbox for specific page
pager: true # Enable pager navigation (prev/next) for specific page
toc: false # Enable Table of Contents for specific page
---

Tous les grimpeurs de Grenoble en ont un jour parlé, mais rares sont ceux qui l'ont vraiment fait. Le Groupe Perf réalisera-t-il ce projet dont tout le monde rêve ?

On parle bien évidemment de louer un garage (ou une cave, ou un grenier...) permettant d'accueillir une structure d'entraînement à la hauteur des ambitions du groupe !

Quelques questions légitimes :

- mais comment il tiendra le pan ?
- Rémy il veut vraiment tout remplir avec des fissures ?
- ça coûte cher des prises non ?
- pourquoi Gaétan a ramené son lit ?
- c'est pas un peu bas de plafond ?
- pourquoi la moitié de l'espace est occupé par un stock de pastis ?
- vous êtes sûr qu'il est pas porteur ce mur ?

Une seule réponse à toutes ces questions: **le déni !**

**[Affaire à suivre...]**
