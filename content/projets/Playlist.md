---
title: "Playlist"
categories:
  - "Projets"

# Theme-Defined params
thumbnail: "img/projets/playlist/playlist.webp" # Thumbnail image
lead: "La performance c'est 50% de force et 50% de musique." # Lead text
comments: false # Enable Disqus comments for specific page
authorbox: false # Enable authorbox for specific page
pager: true # Enable pager navigation (prev/next) for specific page
toc: false # Enable Table of Contents for specific page
---

Même si aucune étude ne le prouve, tout grimpeur de moins de 30 ans sait qu'une musique adaptée permet de gagner environ 1 cotation. C'est scientifique et pas négligeable !

C'est pourquoi le groupe perf a créé une playlist collaborative visant à maximiser les performances à l'entraînement et lors des tentatives d'enchaînement.

Vous pouvez retrouver cette playlist sur:

## Deezer

<iframe title="deezer-widget" src="https://widget.deezer.com/widget/auto/playlist/10684709902" width="100%" height="150" frameborder="0" allowtransparency="true" allow="encrypted-media; clipboard-write"></iframe>

## Spotify

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/playlist/47RBp4Zik8foL1vuNcRrgS?utm_source=generator" width="100%" height="80" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>
