---
title: "Nathan"
categories:
  - "Équipe"

# Theme-Defined params
thumbnail: "img/equipe/Nathan.webp" # Thumbnail image
lead: "La caution perf du groupe perf." # Lead text
comments: false # Enable Disqus comments for specific page
authorbox: false # Enable authorbox for specific page
pager: true # Enable pager navigation (prev/next) for specific page
toc: false # Enable Table of Contents for specific page
---

