---
title: "Marjorie"
categories:
  - "Équipe"

# Theme-Defined params
thumbnail: "img/equipe/Marjorie.webp" # Thumbnail image
lead: "Un mental d'acier dans un squelette en carton." # Lead text
comments: false # Enable Disqus comments for specific page
authorbox: false # Enable authorbox for specific page
pager: true # Enable pager navigation (prev/next) for specific page
toc: false # Enable Table of Contents for specific page
---
