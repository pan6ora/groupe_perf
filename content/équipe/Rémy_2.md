---
title: "Rémy"
categories:
  - "Équipe"

# Theme-Defined params
thumbnail: "img/equipe/Rémy_2.webp" # Thumbnail image
lead: "La légende: rares sont ceux qui l'ont déjà aperçu à l'entraînement." # Lead text
comments: false # Enable Disqus comments for specific page
authorbox: false # Enable authorbox for specific page
pager: true # Enable pager navigation (prev/next) for specific page
toc: false # Enable Table of Contents for specific page
---