---
title: "Mathias"
categories:
  - "Équipe"

# Theme-Defined params
thumbnail: "img/equipe/Mathias.webp" # Thumbnail image
lead: "Skater ou grimpeur ? Le doute demeure." # Lead text
comments: false # Enable Disqus comments for specific page
authorbox: false # Enable authorbox for specific page
pager: true # Enable pager navigation (prev/next) for specific page
toc: false # Enable Table of Contents for specific page
---

