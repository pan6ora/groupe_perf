---
title: "Brice"
categories:
  - "Équipe"

# Theme-Defined params
thumbnail: "img/equipe/Brice.webp" # Thumbnail image
lead: "Son seul anti-style c'est les arachides." # Lead text
comments: false # Enable Disqus comments for specific page
authorbox: false # Enable authorbox for specific page
pager: true # Enable pager navigation (prev/next) for specific page
toc: false # Enable Table of Contents for specific page
---
