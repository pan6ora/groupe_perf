---
title: "Rémy"
categories:
  - "Équipe"

# Theme-Defined params
thumbnail: "img/equipe/Rémy.webp" # Thumbnail image
lead: "Charles Albert, le niveau de grimpe en moins." # Lead text
comments: false # Enable Disqus comments for specific page
authorbox: false # Enable authorbox for specific page
pager: true # Enable pager navigation (prev/next) for specific page
toc: false # Enable Table of Contents for specific page
---

---

