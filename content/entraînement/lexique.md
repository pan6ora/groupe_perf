---
title: "Lexique"
categories:
  - "Entraînement"

# Theme-Defined params
lead: "Des définitions et notions à avoir en tête" # Lead text
comments: false # Enable Disqus comments for specific page
authorbox: false # Enable authorbox for specific page
pager: true # Enable pager navigation (prev/next) for specific page
toc: true # Enable Table of Contents for specific page
summary: " "
---

## Régimes de contraction

**Concentrique**

Effort en fermeture (rapprochement des segments musculaires).\
ex: traction

**Excentrique**

Effort en contrôle d'ouverture (éloignement des segments musculaires).\
ex: descente contrôlée sur pan gullich

**Isométrique**

Effort en blocage (maintient de la distance des segments musculaires).\
ex: blocage 90°

## Filières énergétiques

**Anaérobie alactique**

« La force pure »

Instantanée mais courtes (10sec) car on crame les réserves d’ adénosine TRI phosphate (ATP) contenu dans la cellule musculaire.

Développe une grande puissance sans production de déchet (type lactates)

**Anaérobie lactique**

La « rési »

Elle fonctionne de 10 sec après le début de l’effort pendant 3 à 15 min (ça dépend de votre VO2 max donc de votre PPG)

Elle consomme beaucoup de glucide pour peu d’oxygène ,donc il y a une forte production de déchet comme les lactates (les bouteilles).

**Aérobie**

La « conti »

C’est la dernière filière elle s’enclenche après la précédente (AL)

On re synthétise l’ATP grâce à l’oxygène (1 molécule de glucide donne 38 ATP ) et on peut même utiliser des lipides.

On produit des déchets moins embêtant que l’acide lactique comme du CO2 et de l’H2O.

## Physique

**Souplesse**

Capacité du corps à réaliser un mouvement avec une importante amplitude. Capacité "élastique" des muscles et tendons.

**Mobilité**

Aptitude physique à réaliser des mouvements fonctionnels en série. En comparaison de la souplesse, c'est la capacité à réaliser sois-même un effort avec une importante amplitude.

**Force de contact**

Capacité à générer un haut niveau de force le plus vite possible. Correspond à la vitesse de montée en force, c'est à dire le temps pour les fibres musculaires de synchroniser leur contraction.
ex: tenir une petite prise à l'arrivée d'un mouvement dynamique 

## Théorie de l'entraînement

**Surcompensation**

Le principe de surcompensation est le phénomène qui permet à l’organisme, après avoir subi un stress, de développer une capacité fonctionnelle supérieure. C’est un mécanisme de réadaptation qui permet, après avoir réalisé une période de récupération, de générer un plus haut niveau de performance.

## Sources et inspirations

- [saintjeoireescalade](https://saintjeoireescalade.wordpress.com/quelques-regles-de-securites-2/les-bases-de-lentrainement/)





