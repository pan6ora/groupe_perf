---
title: "Banque d'exercices"
categories:
  - "Entraînement"
lead: "Liste d'exercices classés par filière"
comments: false
authorbox: false
pager: false
toc: true
summary: " "
---

## Échauffement

### Routine Rémy

Description: échauffement général, mélange de plusieurs inspirations dont la méthode Williams Belle rapide.

Durée: 20min

Échauffement général (5min)
```
{{< details >}}
30s - rotation cou
15s - rotation épaule intérieur bras fléchis
15s - rotation épaule extérieur bras fléchis
15s - rotation épaule intérieur bras tendus
15s - rotation épaule extérieur bras tendus
60s - poignets et avants-bras
15s - rotation bassin sens horaire
15s - rotation bassin sens anti-horaire
15s - rotation genou sens horaire
15s - rotation genou sens anti-horaire
30s - rotation cheville G
30s - rotation cheville D
15s - talon-pointe G
15s - talon-pointe D
{{< /details >}}
```
Méthode Williams Belle (5min)
```
{{< details >}}
30s - cosack
30s - coreflow 6 steps
30s - accroupi tendu
30s - ouverture
20s - bascu 90°
20s - pompes
20s - swimmer
20s - superman
20s - hindi push-up
20s - squat kick
15s - balancement avant-arrière jambe G
15s - balancement avant-arrière jambe D
15s - balancement latéral jambe G
15s - balancement latéral jambe D
{{< /details >}}
```
Échauffement doigts (5min)
```
{{< details >}}
prise 15mm: 5s,10r,5s,10r,5s,10r,5s,5r,5s
1min r
prise 15mm: 10s,15r,10s,15r,10s
1min r
prise 10mm: 5s,10r,5s,10r,10s,10r,10s
{{< /details >}}
```
Échauffement spécifique (5min)
```
{{< details >}}
5min grimpe continue sur prises bonnes à moyennes (se concentrer sur l'écoute et la gestuelle)
{{< /details >}}
```

## PPG / Gainage

### circuit PPG groupe perf

Description: série d'exercices de PPG (force/gainage/cardio)

Durée: 10:30

Format
```
{{< details >}}
[ (30s + 30s R) x9 + 5min R ] x2

tractions - montées escalier - pompes - corde à sauter - gainage - squats sautés - dips - araignée - levée de poids
{{< /details >}}
```

### circuit gainage groupe perf

Description: alternance d'exercices de gainage face / dos

Durée: 06:40

Format
```
{{< details >}}
[ (20s +5s R) x8 + 5s R ] x2

rameur - face monté bras - dos toucher chevilles - face monté genou extérieur - chandelle - face monté genou intérieur - insecte - araignée
{{< /details >}}
```

### circuit gainage fonctionnel

Description: 3 sets de 3 exos de gainage fonctionnel

Durée: ~08:00

Format
```
{{< details >}}
[ (8rep +30s R) x3 + 2min R ] x3

1.
2.
3. 
{{< /details >}}
```

## Souplesse / Mobilité

### routine souplesse Rémy

Description: routine d'assouplissement (à faire tous les soirs)

Durée: 14:05

Format
```
{{< details >}}
(15s + 5s R + 60s + 5s R) x10

papillon - grand écart - toucher pieds - levrette - fente avant G - fente avant D - papillon - grand écart - toucher pieds - levrette
{{< /details >}}
```

## Rési / Conti

## Force / Puissance / Force de contact

## Doigts

## Technique /Stratégie / Mental